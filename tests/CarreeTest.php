<?php

use Dev\Demo206\Carree;
use PHPUnit\Framework\TestCase;

class CarreeTest extends TestCase
{


    public function testSurface()
    {
        $object = new Carree(5);
        $this->assertEquals(25, $object->surface());
    }
    public function testPerimetre()
    {
        $object = new Carree(5);
        $this->assertEquals(20, $object->perimetre());
    }

    public function testSetCote()
    {
        $this->expectException(Exception::class);
        $object = new Carree(-5);
    }
}
