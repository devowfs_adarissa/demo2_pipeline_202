<?php

namespace Dev\Demo206;

use Exception;

class Carree
{
    private $cote;

    public function __construct($cote)
    {
        $this->setCote($cote);
    }
    public function surface()
    {
        return $this->cote * $this->cote;
    }


    public function setCote($cote)
    {
        if ($cote < 0) throw new Exception("Cote invalide!");
        $this->cote = $cote;
    }

    public function perimetre()
    {
        if ($this->cote > 0)
            return $this->cote * 4;
    }
}
